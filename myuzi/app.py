#!/usr/bin/env python3

import importlib.metadata, logging, random, time

from myuzi import cache, playlists, utils, settings

import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Gst', '1.0')
from gi.repository import Gtk, Gdk, GLib, Gst, Pango


### --- DATA CLASSES --- ###


class Interface:
	window: Gtk.ApplicationWindow
	ntb_main: Gtk.Notebook
	box_results: Gtk.Box
	box_playlists: Gtk.Box
	lbl_title: Gtk.Label
	bar_progress: Gtk.ProgressBar
	btn_pause: Gtk.Button
	collapsed_playlists = [name for name in playlists.read_playlists()]


class Player:
	playbin: Gst.Element = None
	playbin_lock = GLib.Mutex()
	index: int = 0
	skip_segments: dict = {}
	queue: list = []
	loop: bool = False
	shuffle: bool = False
	search_results: list = []
	playback_retries: int = 0
	online: bool = utils.is_yt_available()
	downloading: bool = False
	searching: bool = False


### --- MISC --- ###


# handle async song playback and sync interface update
def play_song(id_: str, title: str, author: str):
	logging.info(f'Playing {title} by {author}, id {id_}')

	# update player interface
	Interface.btn_pause.set_icon_name('media-playback-pause')
	Interface.lbl_title.set_text(title)
	Interface.window.set_sensitive(False)

	def playback_thread(id_: str, title: str, author: str):
		# reset playbin and prep for playback
		Player.playbin_lock.lock()
		remake_playbin()
		Player.playbin.set_state(Gst.State.READY)
		Player.playbin.set_property('uri', utils.get_song_uri(id_))

		# get skip segments
		Player.skip_segments = {}
		if not cache.is_song_cached(id_):
			skips = utils.get_skip_segments(id_)
			if skips is not None: # {} is ok, None is error
				Player.skip_segments = skips
				logging.info(f'Skip segments: {Player.skip_segments}')
			else:
				Player.skip_segments = {}
				logging.error('Failed to get skip segments')

		# set up bus
		bus = Player.playbin.get_bus()
		bus.add_signal_watch()
		bus.connect('message::error', _on_bus_error, id_, title, author)
		bus.connect('message::eos', _on_song_end)

		# play stream
		Player.playbin.set_state(Gst.State.PLAYING)
		Player.playbin_lock.unlock()

	if Player.online or cache.is_song_cached(id_):
		GLib.Thread.new(None, playback_thread, id_, title, author)
	else:
		logging.info(f'Skipping {title} by {author}, id {id_} - offline mode')
		_on_next_song(None)


# moves progress bar and auto-skips segments
def update_progress() -> True:
	# low priority, so just give up if can't lock.
	# otherwise the interface would freeze
	if not Player.playbin_lock.trylock():
		Interface.window.set_sensitive(False)
		return True
	elif not Player.downloading and not Player.searching:
		Interface.window.set_sensitive(True)

	duration = Player.playbin.query_duration(Gst.Format.TIME)[1]
	position = Player.playbin.query_position(Gst.Format.TIME)[1]

	# only do things if playing
	if duration > 0:
		Interface.bar_progress.set_fraction(position / duration)
		round_pos_seconds = round(position / Gst.SECOND)
		if round_pos_seconds in Player.skip_segments:
			Player.playbin.seek_simple(
				Gst.Format.TIME,
				Gst.SeekFlags.FLUSH | Gst.SeekFlags.ACCURATE,
				Player.skip_segments[round_pos_seconds] * Gst.SECOND
			)
	else:
		Interface.bar_progress.set_fraction(0)

	Player.playbin_lock.unlock()
	return True


def remake_playbin():
	# clean delete
	if Player.playbin:
		Player.playbin.set_state(Gst.State.NULL)
		del Player.playbin

	# new playbin
	Player.playbin = Gst.ElementFactory.make('playbin', 'playbin')
	Player.playbin.set_state(Gst.State.READY)
	Player.playbin.set_property('volume', float(settings.get_value('volume', 1)))
	Player.playbin.set_property('mute', False)


def play_radio_song():
	if not Player.online or not int(settings.get_value('radio')):
		return

	# show status
	Interface.window.set_title(_('Searching for a song to play...'))
	Interface.window.set_sensitive(False)

	id_queue = [s['id'] for s in Player.queue]
	random.shuffle(id_queue)

	# thread to find and play a song
	def radio_search():
		Player.playbin_lock.lock()
		song = None
		for id_ in id_queue:
			song = utils.get_similar_song(id_, ignore = id_queue)
			if song:
				logging.info('Playing recommended song')
				break
		else: # nobreak
			logging.info('Failed to find similar song, replaying random')
			song = random.choice(Player.queue)

		Player.queue.append(song)
		Player.index += 1
		Player.playback_retries = 0
		play_song(song['id'], song['title'], song['author'])
		Player.playbin_lock.unlock()

	# timeout to unlock interface when done
	def post_radio_search_check() -> bool:
		# i wanted to pulse() here but it just refused to work
		if Player.playbin_lock.trylock():
			Player.playbin_lock.unlock()

			Interface.window.set_title('Myuzi')
			Interface.window.set_sensitive(True)
			return False

		return True

	GLib.timeout_add_seconds(1, post_radio_search_check)
	GLib.Thread.new('radio search thread', radio_search)


### --- EVENT FUNCTIONS --- ###


# stream error event (usually 403)
def _on_bus_error(_, err, song_id: str, song_title: str, song_author: str):
	err = err.parse_error()
	logging.error(f'Bus error: {err}')

	# delete everything, retry from scratch
	remake_playbin()

	# wait before retrying - if it's 403 yt might block again
	time.sleep(2)

	if Player.playback_retries > 1:
		# inform user of error
		logging.error('Failed to recover from bus error')
		msg_dialog = Gtk.MessageDialog(
			transient_for = Interface.window,
			destroy_with_parent = True,
			modal = True,
			buttons = Gtk.ButtonsType.OK,
			text = _('Playback error'),
			secondary_text = f'<{song_id}>: {err.gerror}'
		)
		msg_dialog.connect('response', lambda d, _: d.destroy())
		msg_dialog.show()

		# clean up
		Interface.bar_progress.set_fraction(0)
		Interface.lbl_title.set_text('...')
		Interface.btn_pause.set_icon_name('media-playback-start')
		Player.playback_retries = 0
		return

	logging.warn('Retrying playback')
	Player.playback_retries += 1
	play_song(song_id, song_title, song_author)


# stream end event, play next in queue
def _on_song_end(*_):
	# avoid receiving multiple eos signals
	if Player.playbin.get_bus().have_pending():
		return

	# all is well
	Player.playback_retries = 0

	# get rid of playbin, make a new one
	remake_playbin()

	# reset interface
	Interface.bar_progress.set_fraction(0)
	Interface.lbl_title.set_text('...')

	# loop, shuffle, queue
	song = None
	queue_length = len(Player.queue)
	if Player.loop:
		logging.info('Repeating current song')
		song = Player.queue[Player.index]
	elif Player.shuffle and queue_length > 1:
		logging.info('Playing random song from queue')

		# don't play the same song twice in a row
		random_index = 0
		while True:
			random_index = random.randrange(0, queue_length)
			if random_index != Player.index:
				break

		Player.index = random_index
		song = Player.queue[Player.index]
	elif queue_length - 1 > Player.index:
		logging.info('Playing next song from queue')
		Player.index += 1
		song = Player.queue[Player.index]

	# loop/shuffle or queue is not over
	if song:
		play_song(song['id'], song['title'], song['author'])
		return

	# nothing to play, let radio handle this
	logging.info('End of queue reached')
	play_radio_song()


# play/pause button click event
def _on_toggle_pause(_):
	# inf timeout, second element of returned thing is the actual state
	state = Player.playbin.get_state(Gst.CLOCK_TIME_NONE)[1]

	if state == Gst.State.PLAYING:
		Player.playbin.set_state(Gst.State.PAUSED)
		Interface.btn_pause.set_icon_name('media-playback-start')
	else:
		Player.playbin.set_state(Gst.State.PLAYING)
		Interface.btn_pause.set_icon_name('media-playback-pause')


# next song button click event
def _on_next_song(_):
	logging.info('Skipping to next song')

	# all is well
	Player.playback_retries = 0

	# get rid of playbin, make a new one
	remake_playbin()

	# reset interface
	Interface.bar_progress.set_fraction(0)
	Interface.lbl_title.set_text('...')

	# shuffle?
	queue_length = len(Player.queue)
	song = None
	if Player.shuffle and queue_length > 1:
		logging.info('Playing random song from queue')

		# don't play the same song twice in a row
		random_index = 0
		while True:
			random_index = random.randrange(0, queue_length)
			if random_index != Player.index:
				break

		Player.index = random_index
		song = Player.queue[Player.index]
	elif queue_length - 1 > Player.index :
		Player.index += 1
		Player.playback_retries = 0
		song = Player.queue[Player.index]

	if song:
		play_song(song['id'], song['title'], song['author'])
		return

	# find and play new song
	logging.info('End of queue reached')
	play_radio_song()


# previous song button click event
def _on_previous_song(_):
	Player.playbin.set_state(Gst.State.READY)
	Player.index -= 1
	Player.playback_retries = 0

	# will restart song if nothing before it in queue
	if Player.index < 0:
		Player.index = 0

	# play the thing if queue not empty
	if len(Player.queue) > 0:
		song = Player.queue[Player.index]
		play_song(song['id'], song['title'], song['author'])


# play queue starting at index
def _on_play_queue(_, queue: list, index: int):
	logging.info('New queue')

	if not Player.online:
		queue = [song for song in queue if cache.is_song_cached(song['id'])]

	if len(queue) == 0:
		logging.info('Not playing, empty queue')
		return

	Player.queue = queue
	Player.index = index
	Player.playback_retries = 0
	song = queue[index]
	play_song(song['id'], song['title'], song['author'])


# swap songs in playlist event
def _on_swap_in_playlist(btn: Gtk.Button, i: int, j: int, playlist: str):
	btn.get_parent().get_parent().get_parent().popdown()
	playlists.swap_songs(i, j, playlist)
	build_playlists()


# add song to playlist event
def _on_add_to_playlist(btn: Gtk.Button, id_: str, title: str, author: str, list_: str):
	btn.get_parent().get_parent().get_parent().popdown()
	playlists.add_song(id_, title, author, list_)
	# don't rebuild playlists, because we're in the search tab anyway


# add currently playing song to playlist
def _on_add_current_to_playlist(btn: Gtk.Button, list_: str):
	btn.get_parent().get_parent().get_parent().popdown()
	if len(Player.queue) > Player.index:
		song = Player.queue[Player.index]
		playlists.add_song(song['id'], song['title'], song['author'], list_)

	if Interface.ntb_main.get_current_page() == 0:
		build_playlists()


# add currently playing song
def _on_add_current_to_new_playlist(ent: Gtk.Entry):
	if len(Player.queue) > Player.index:
		_on_create_playlist(ent, Player.queue[Player.index])

	ent.set_text('')
	if Interface.ntb_main.get_current_page() == 0:
		build_playlists()


# rename song in playlist event
def _on_rename_song(ent: Gtk.Entry, index: int, playlist: str):
	ent.get_parent().get_parent().get_parent().popdown()

	playlists.rename_song(index, playlist, ent.get_text())
	build_playlists()


# save song to local storage event
def _on_cache_song(btn: Gtk.Button, id_: str):
	btn.get_parent().get_parent().get_parent().popdown()

	Interface.window.set_sensitive(False)
	Interface.window.set_title(_('Downloading song...'))
	Player.downloading = True

	def post_cache_unlock(id_: str):
		# done
		if not Player.downloading:
			# download failed
			if not cache.is_song_cached(id_):
				msg_dialog = Gtk.MessageDialog(
					transient_for = Interface.window,
					destroy_with_parent = True,
					modal = True,
					buttons = Gtk.ButtonsType.OK,
					text = _('Download error'),
					secondary_text = _('Song download failed')
				)
				msg_dialog.connect('response', lambda d, _: d.destroy())
				msg_dialog.show()

			# cleanup
			Interface.window.set_title('Myuzi')
			Interface.window.set_sensitive(True)
			build_playlists()
			return False

		return True

	GLib.Thread.new('cache thread', cache.cache_song, id_, Player)
	GLib.timeout_add_seconds(1, post_cache_unlock, id_)


# remove song from local storage event
def _on_uncache_song(btn: Gtk.Button, id_: str):
	btn.get_parent().get_parent().get_parent().popdown()
	cache.uncache_song(id_)
	build_playlists()


# remove song from playlist event
def _on_remove_from_playlist(btn: Gtk.Button, index: int, playlist: str):
	btn.get_parent().get_parent().get_parent().popdown()

	song_id = playlists.read_playlists()[playlist][index]['id']
	playlists.remove_song(index, playlist)

	# only uncache if the song is not in any other playlists
	if not playlists.is_song_in_any_playlist(song_id):
		cache.uncache_song(song_id)

	build_playlists()


# create playlist with song event
def _on_create_playlist(entry: Gtk.Entry, song: dict):
	entry.get_parent().get_parent().get_parent().popdown()

	name = entry.get_text().strip()
	entry.set_text('')
	if playlists.add_playlist(name):
		playlists.add_song(song['id'], song['title'], song['author'], name)
		build_search()
	else:
		msg_dialog = Gtk.MessageDialog(
			transient_for = Interface.window,
			destroy_with_parent = True,
			modal = True,
			buttons = Gtk.ButtonsType.OK,
			text = _('Playlist creation failed'),
			secondary_text = _('Playlist already exists')
		)
		msg_dialog.connect('response', lambda d, _: d.destroy())
		msg_dialog.show()


# import playlist event
def _on_import_playlist(btn: Gtk.Button):
	def do_import(clipboard: object, task: object):
		try:
			data = clipboard.read_text_finish(task)
			name = f'Imported {data[-5:]}{time.time()}'
			success = playlists.import_playlist(name, data)
		except gi.repository.GLib.GError:
			success = False

		if success:
			build_playlists()
		else:
			msg_dialog = Gtk.MessageDialog(
				transient_for = Interface.window,
				destroy_with_parent = True,
				modal = True,
				buttons = Gtk.ButtonsType.OK,
				text = _('Playlist import failed'),
				secondary_text = _('No playlist data in clipboard')
			)
			msg_dialog.connect('response', lambda d, _: d.destroy())
			msg_dialog.show()

	btn.get_clipboard().read_text_async(callback = do_import)


# rename playlist event
def _on_rename_playlist(ent: Gtk.Entry, name: str):
	ent.get_parent().get_parent().get_parent().popdown()

	if playlists.rename_playlist(name, ent.get_text()):
		build_playlists()
	else:
		msg_dialog = Gtk.MessageDialog(
			transient_for = Interface.window,
			destroy_with_parent = True,
			modal = True,
			buttons = Gtk.ButtonsType.OK,
			text = _('Playlist rename failed'),
			secondary_text = _('Playlist already exists')
		)

		msg_dialog.connect('response', lambda d, _: d.destroy())
		msg_dialog.show()


# share playlist event
def _on_share_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()

	btn.get_clipboard().set_content(
		Gdk.ContentProvider.new_for_value(playlists.export_playlist(name))
	)
	msg_dialog = Gtk.MessageDialog(
		transient_for = Interface.window,
		destroy_with_parent = True,
		modal = True,
		buttons = Gtk.ButtonsType.OK,
		text = _('Exported'),
		secondary_text = _('Playlist data copied to clipboard')
	)
	msg_dialog.connect('response', lambda d, _: d.destroy())
	msg_dialog.show()


# expand playlist event
def _on_expand_playlist(button: Gtk.Button, box: Gtk.Box, name: str):
	visible = box.get_visible()
	box.set_visible(not visible)
	button.set_label(('▶ ' if visible else '▼ ') + name)
	if visible:
		if name not in Interface.collapsed_playlists:
			Interface.collapsed_playlists.append(name)
	elif name in Interface.collapsed_playlists:
		Interface.collapsed_playlists.remove(name)


def _on_cache_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()

	Interface.window.set_sensitive(False)
	Interface.window.set_title(_('Downloading playlist...'))
	Player.downloading = True
	songs = playlists.read_playlists()[name]

	def cache_progress(songs: list):
		# calculate progress
		cached = 0
		for song in songs:
			if cache.is_song_cached(song['id']):
				cached += 1

		# done
		if cached == len(songs) or not Player.downloading:
			# download failed?
			if cached != len(songs):
				msg_dialog = Gtk.MessageDialog(
					transient_for = Interface.window,
					destroy_with_parent = True,
					modal = True,
					buttons = Gtk.ButtonsType.OK,
					text = _('Download error'),
					secondary_text = _('Playlist download failed')
				)
				msg_dialog.connect('response', lambda d, _: d.destroy())
				msg_dialog.show()

			# cleanup
			Interface.window.set_title('Myuzi')
			build_playlists()
			Interface.window.set_sensitive(True)
			return False

		return True

	GLib.Thread.new('cache thread', cache.cache_playlist, name, Player)
	GLib.timeout_add(400, cache_progress, songs)


def _on_uncache_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()
	cache.uncache_playlist(name)
	build_playlists()


# delete playlist event
def _on_delete_playlist(btn: Gtk.Button, name: str):
	btn.get_parent().get_parent().get_parent().popdown()

	playlist = playlists.read_playlists()[name].copy()
	playlists.remove_playlist(name)

	# uncache songs that only appear in this playlist
	for song in playlist:
		if not playlists.is_song_in_any_playlist(song['id']):
			cache.uncache_song(song['id'])

	build_playlists()


# search event
def _on_search(entry):
	if not Player.online:
		msg_dialog = Gtk.MessageDialog(
			transient_for = Interface.window,
			destroy_with_parent = True,
			modal = True,
			buttons = Gtk.ButtonsType.OK,
			text = _('Unavailable'),
			secondary_text = _('Can\'t search in offline mode')
		)
		msg_dialog.connect('response', lambda d, _: d.destroy())
		msg_dialog.show()
		return

	def do_search(query: str):
		Player.search_results = utils.find_songs(query)
		Player.searching = False

	def search_progress() -> bool:
		if Player.searching:
			return True

		build_search()
		Interface.window.set_sensitive(True)
		Interface.window.set_title('Myuzi')
		return False

	Interface.window.set_title(_('Searching...'))
	Interface.window.set_sensitive(False)
	Player.searching = True
	GLib.Thread.new('search thread', do_search, entry.get_text())
	GLib.timeout_add_seconds(1, search_progress)


def _on_toggle_online(toggle: Gtk.ToggleButton):
	Player.online = False
	toggle.set_label(_('Offline'))

	if toggle.get_active():
		if utils.is_yt_available():
			Player.online = True
			toggle.set_label(_('Online'))
		else:
			toggle.set_active(False)
			msg_dialog = Gtk.MessageDialog(
				transient_for = Interface.window,
				destroy_with_parent = True,
				modal = True,
				buttons = Gtk.ButtonsType.OK,
				text = _('Can\'t switch to online mode'),
				secondary_text = _('Connection failed')
			)
			msg_dialog.connect('response', lambda d, _: d.destroy())
			msg_dialog.show()

	build_search()
	build_playlists()


# switch tab event
def _on_switch_page(_, page, page_num):
	if page_num == 0:
		build_playlists()
	elif page_num == 1:
		build_search()


# volume change event, from settings
def _on_volume_change(scale: Gtk.Scale):
	volume = scale.get_value()
	Player.playbin.set_property('volume', volume)
	settings.set_value('volume', volume)


# radio autoplay toggled event, from settings
def _on_radio_toggle(toggle: Gtk.CheckButton):
	settings.set_value('radio', int(toggle.get_active()))


def _on_shuffle_toggle(toggle: Gtk.ToggleButton):
	Player.shuffle = toggle.get_active()


def _on_loop_toggle(toggle: Gtk.ToggleButton):
	Player.loop = toggle.get_active()


### --- INTERFACE BUILDERS --- ###


# (re)builds search tab interface
def build_search():
	# clear old results
	while True:
		child = Interface.box_results.get_first_child()
		if child:
			Interface.box_results.remove(child)
		else:
			break

	# write new results (don't ask yt every time!)
	for song in Player.search_results:
		# clickable song title (plays song)
		btn_song = Gtk.Button.new_with_label(f'{song["title"]} ({song["author"]})')
		btn_song.set_tooltip_text(_('Play'))
		btn_song.set_hexpand(True)
		btn_song.set_has_frame(False)
		btn_song.get_first_child().set_halign(Gtk.Align.START)
		btn_song.get_first_child().set_ellipsize(Pango.EllipsizeMode.END)
		btn_song.connect('clicked', _on_play_queue, [song], 0)

		if not Player.online and not cache.is_song_cached(song['id']):
			btn_song.set_sensitive(False)

		# add to playlist menu
		box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
		box_pop.set_spacing(5)
		for playlist in playlists.read_playlists().keys():
			btn_playlist = Gtk.Button.new_with_label(playlist)
			btn_playlist.set_has_frame(False)
			btn_playlist.connect(
				'clicked',
				_on_add_to_playlist,
				song['id'],
				song['title'],
				song['author'],
				playlist
			)
			box_pop.append(btn_playlist)

		child = box_pop.get_first_child()
		if child:
			child.connect('map', lambda w: w.grab_focus())

		ent_name = Gtk.Entry()
		ent_name.set_placeholder_text(_('New playlist...'))
		ent_name.connect('activate', _on_create_playlist, song)
		box_pop.prepend(ent_name)

		pop_add = Gtk.Popover.new()
		pop_add.set_child(box_pop)

		# add to playlist button
		btn_add = Gtk.MenuButton()
		btn_add.set_has_frame(False)
		btn_add.set_tooltip_text(_('Add to playlist'))
		btn_add.set_icon_name('list-add')
		btn_add.set_popover(pop_add)

		# box for single song
		box_result = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
		box_result.set_spacing(5)
		box_result.set_hexpand(True)
		box_result.set_halign(Gtk.Align.FILL)
		box_result.append(btn_add)
		box_result.append(btn_song)
		Interface.box_results.append(box_result)


# (re)builds playlists tab interface
def build_playlists():
	# for use with Interface.collapsed_playlists. widget:playlist_name
	hideable_boxes = {}

	# clear playlists for reload
	while True:
		child = Interface.box_playlists.get_first_child()
		if child:
			Interface.box_playlists.remove(child)
		else:
			break

	# box for playlists
	box_lists = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_lists.set_spacing(5)
	box_lists.set_margin_bottom(5)
	box_lists.set_hexpand(True)

	# all playlists
	for name, content in playlists.read_playlists().items():
		# expand playlist button
		btn_expand = Gtk.Button.new_with_label(
			('▶ ' if name in Interface.collapsed_playlists else '▼ ') + name
		)
		btn_expand.get_first_child().set_ellipsize(Pango.EllipsizeMode.END)
		btn_expand.get_first_child().set_halign(Gtk.Align.START)
		btn_expand.set_halign(Gtk.Align.FILL)
		btn_expand.set_hexpand(True)
		btn_expand.set_has_frame(False)
		# note that .connect happens further down

		# menu with more playlist actions
		box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
		box_pop.set_spacing(5)
		btn_delete = Gtk.Button.new_with_label(_('Delete'))
		btn_delete.set_has_frame(False)
		btn_delete.connect('clicked', _on_delete_playlist, name)
		btn_uncache = Gtk.Button.new_with_label(_('Remove from local storage'))
		btn_uncache.set_has_frame(False)
		btn_uncache.connect('clicked', _on_uncache_playlist, name)
		btn_share = Gtk.Button.new_with_label(_('Export to clipboard'))
		btn_share.set_has_frame(False)
		btn_share.connect('clicked', _on_share_playlist, name)
		btn_share.connect('map', lambda w: w.grab_focus())
		ent_name = Gtk.Entry()
		ent_name.set_text(name)
		ent_name.connect('activate', _on_rename_playlist, name)
		box_pop.append(ent_name)
		box_pop.append(btn_share)
		if Player.online:
			btn_cache = Gtk.Button.new_with_label(_('Save to local storage'))
			btn_cache.set_has_frame(False)
			btn_cache.connect('clicked', _on_cache_playlist, name)
			box_pop.append(btn_cache)
		box_pop.append(btn_uncache)
		box_pop.append(btn_delete)

		pop_more = Gtk.Popover.new()
		pop_more.set_child(box_pop)

		# button for "more" menu
		btn_more = Gtk.MenuButton()
		btn_more.set_icon_name('view-more')
		btn_more.set_has_frame(False)
		btn_more.set_popover(pop_more)

		# box for playlist header
		box = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
		box.set_spacing(5)
		box.set_halign(Gtk.Align.FILL)
		box.set_valign(Gtk.Align.START)
		box.set_hexpand(True)
		box.append(btn_expand)
		box.append(btn_more)
		box_lists.append(box)

		# box for all songs in playlist
		box_songs = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
		box_songs.set_spacing(5)
		hideable_boxes[box_songs] = name
		btn_expand.connect('clicked', _on_expand_playlist, box_songs, name)

		# all playlist songs
		for i, song in enumerate(content):
			# clickable song title (plays playlist starting with song)
			btn_song = Gtk.Button.new_with_label(song['title'])
			btn_song.set_tooltip_text(_('Play'))
			btn_song.set_hexpand(True)
			btn_song.set_has_frame(False)
			btn_song.get_first_child().set_ellipsize(Pango.EllipsizeMode.END)
			btn_song.get_first_child().set_halign(Gtk.Align.START)
			btn_song.connect('clicked', _on_play_queue, content, i)

			# "more" menu - song actions
			box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
			box_pop.set_spacing(5)
			ent_name = Gtk.Entry()
			ent_name.set_text(song['title'])
			ent_name.connect('activate', _on_rename_song, i, name)
			box_pop.append(ent_name)
			btn_up, btn_down, btn_uncache, btn_cache = None, None, None, None
			if i != 0:
				btn_up = Gtk.Button.new_with_label(_('Move up'))
				btn_up.set_has_frame(False)
				btn_up.connect('clicked', _on_swap_in_playlist, i, i - 1, name)
				box_pop.append(btn_up)
			if i != len(content) - 1:
				btn_down = Gtk.Button.new_with_label(_('Move down'))
				btn_down.set_has_frame(False)
				btn_down.connect('clicked', _on_swap_in_playlist, i, i + 1, name)
				box_pop.append(btn_down)
			if cache.is_song_cached(song['id']):
				btn_uncache = Gtk.Button.new_with_label(_('Remove from local storage'))
				btn_uncache.set_has_frame(False)
				btn_uncache.connect('clicked', _on_uncache_song, song['id'])
				box_pop.append(btn_uncache)
			elif Player.online:
				btn_cache = Gtk.Button.new_with_label(_('Save to local storage'))
				btn_cache.set_has_frame(False)
				btn_cache.connect('clicked', _on_cache_song, song['id'])
				box_pop.append(btn_cache)
			else:
				btn_song.set_sensitive(False)
			btn_remove = Gtk.Button.new_with_label(_('Remove from playlist'))
			btn_remove.set_has_frame(False)
			btn_remove.connect('clicked', _on_remove_from_playlist, i, name)
			box_pop.append(btn_remove)

			for b in [btn_up, btn_down, btn_uncache, btn_cache]:
				if b:
					b.connect('map', lambda w: w.grab_focus())
					break
			else: # nobreak
				btn_remove.connect('map', lambda w: w.grab_focus())

			pop_more = Gtk.Popover.new()
			pop_more.set_child(box_pop)

			# button for "more" menu
			btn_more = Gtk.MenuButton()
			btn_more.set_has_frame(False)
			btn_more.set_icon_name('view-more')
			btn_more.set_popover(pop_more)

			# box for song
			box = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
			box.set_spacing(5)
			box.set_hexpand(True)
			box.set_halign(Gtk.Align.FILL)
			box.append(btn_more)
			box.append(btn_song)
			box_songs.append(box)

		box_lists.append(box_songs)

	# scroller for box for playlists
	scr_lists = Gtk.ScrolledWindow()
	scr_lists.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
	scr_lists.set_vexpand(True)
	scr_lists.set_hexpand(True)
	scr_lists.set_child(box_lists)

	Interface.box_playlists.append(scr_lists)

	# playlists should stay collapsed
	for widget, name in hideable_boxes.items():
		if name in Interface.collapsed_playlists:
			widget.set_visible(False)


# builds the app's interface. SHOULD ONLY HAPPEN ONCE ON APP INIT
def build(app):
	logging.basicConfig(level = logging.INFO)

	## player
	# current song title/author
	Interface.lbl_title = Gtk.Label()
	Interface.lbl_title.set_halign(Gtk.Align.CENTER)
	Interface.lbl_title.set_ellipsize(Pango.EllipsizeMode.END)
	Interface.lbl_title.set_margin_start(10)
	Interface.lbl_title.set_margin_end(10)
	Interface.lbl_title.set_label('...')

	# song progress thing
	Interface.bar_progress = Gtk.ProgressBar.new()
	Interface.bar_progress.set_show_text(False)
	Interface.bar_progress.set_halign(Gtk.Align.FILL)
	Interface.bar_progress.set_valign(Gtk.Align.END)
	GLib.timeout_add(300, update_progress)

	# playback control buttons
	Interface.btn_pause = Gtk.Button.new_from_icon_name('media-playback-start')
	Interface.btn_pause.set_tooltip_text(_('Toggle pause'))
	Interface.btn_pause.connect('clicked', _on_toggle_pause)
	Interface.btn_pause.set_has_frame(False)
	btn_next = Gtk.Button.new_from_icon_name('media-skip-forward')
	btn_next.set_tooltip_text(_('Next song'))
	btn_next.connect('clicked', _on_next_song)
	btn_next.set_has_frame(False)
	btn_prev = Gtk.Button.new_from_icon_name('media-skip-backward')
	btn_prev.set_tooltip_text(_('Previous song'))
	btn_prev.connect('clicked', _on_previous_song)
	btn_prev.set_has_frame(False)
	tog_loop = Gtk.ToggleButton()
	tog_loop.set_icon_name('media-playlist-repeat')
	tog_loop.set_tooltip_text(_('Repeat this song'))
	tog_loop.set_has_frame(False)
	tog_loop.connect('toggled', _on_loop_toggle)
	tog_shuffle = Gtk.ToggleButton()
	tog_shuffle.set_icon_name('media-playlist-shuffle')
	tog_shuffle.set_has_frame(False)
	tog_shuffle.set_tooltip_text(_('Play random songs from queue'))
	tog_shuffle.connect('toggled', _on_shuffle_toggle)

	# add to playlist menu
	box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_pop.set_spacing(5)
	for playlist in playlists.read_playlists().keys():
		btn_playlist = Gtk.Button.new_with_label(playlist)
		btn_playlist.set_has_frame(False)
		btn_playlist.connect('clicked', _on_add_current_to_playlist, playlist)
		box_pop.append(btn_playlist)

	child = box_pop.get_first_child()
	if child:
		child.connect('map', lambda w: w.grab_focus())

	ent_name = Gtk.Entry()
	ent_name.set_placeholder_text(_('New playlist...'))
	ent_name.connect('activate', _on_add_current_to_new_playlist)
	box_pop.prepend(ent_name)

	pop_add = Gtk.Popover.new()
	pop_add.set_child(box_pop)

	# add to playlist button
	btn_add = Gtk.MenuButton()
	btn_add.set_has_frame(False)
	btn_add.set_tooltip_text(_('Add to playlist'))
	btn_add.set_icon_name('list-add')
	btn_add.set_popover(pop_add)

	# volume control
	lbl_volume = Gtk.Label.new(_('Volume'))
	scl_volume = Gtk.Scale.new_with_range(Gtk.Orientation.HORIZONTAL, 0, 1, 0.1)
	scl_volume.set_hexpand(True)
	scl_volume.set_draw_value(False)
	scl_volume.connect('value-changed', _on_volume_change)

	try:
		v = float(settings.get_value('volume'))
		scl_volume.set_value(v)
	except ValueError:
		scl_volume.set_value(1)
		settings.set_value('volume', 1)

	box_volume = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
	box_volume.set_spacing(5)
	box_volume.set_halign(Gtk.Align.FILL)
	box_volume.set_hexpand(True)
	box_volume.append(lbl_volume)
	box_volume.append(scl_volume)

	# autoplay radio checkbox
	chk_autoplay = Gtk.CheckButton.new_with_label(_('Autoplay similar songs after queue ends'))
	chk_autoplay.get_last_child().set_wrap(True)
	chk_autoplay.connect('toggled', _on_radio_toggle)

	try:
		v = bool(int(settings.get_value('radio')))
		chk_autoplay.set_active(v)
	except ValueError:
		chk_autoplay.set_active(False)
		settings.set_value('radio', 0)

	box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_pop.append(box_volume)
	box_pop.append(chk_autoplay)
	pop_more = Gtk.Popover.new()
	pop_more.set_child(box_pop)

	btn_more = Gtk.MenuButton()
	btn_more.set_icon_name('view-more')
	btn_more.set_popover(pop_more)
	btn_more.set_has_frame(False)

	# box for playback controls
	box_controls = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
	box_controls.set_valign(Gtk.Align.END)
	box_controls.set_halign(Gtk.Align.CENTER)
	box_controls.append(btn_add)
	box_controls.append(tog_shuffle)
	box_controls.append(btn_prev)
	box_controls.append(Interface.btn_pause)
	box_controls.append(btn_next)
	box_controls.append(tog_loop)
	box_controls.append(btn_more)

	# metabox for player
	box_player = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_player.set_spacing(5)
	box_player.append(Interface.lbl_title)
	box_player.append(box_controls)
	box_player.append(Interface.bar_progress)

	## search tab
	# search entry
	ent_search = Gtk.SearchEntry()
	ent_search.connect('activate', _on_search)

	# results box
	Interface.box_results = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	Interface.box_results.set_spacing(5)
	scr_results = Gtk.ScrolledWindow()
	scr_results.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
	scr_results.set_vexpand(True)
	scr_results.set_child(Interface.box_results)

	# search tab
	box_search = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_search.set_spacing(5)
	box_search.set_margin_start(10)
	box_search.set_margin_end(10)
	box_search.set_margin_top(10)
	box_search.set_margin_bottom(10)
	box_search.set_vexpand(True)
	box_search.append(ent_search)
	box_search.append(scr_results)

	## playlists tab
	Interface.box_playlists = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	Interface.box_playlists.set_spacing(5)
	Interface.box_playlists.set_margin_start(10)
	Interface.box_playlists.set_margin_end(10)
	Interface.box_playlists.set_margin_top(10)
	Interface.box_playlists.set_margin_bottom(10)
	Interface.box_playlists.set_vexpand(True)

	## about tab
	# info label
	lbl_info = Gtk.Label.new(
		'Myuzi v' + importlib.metadata.version('myuzi') + ' © zehkira'
	)
	lbl_info.set_halign(Gtk.Align.START)

	# patrons list
	lbl_patrons = Gtk.Label.new(_('Patrons:'))
	lbl_patrons.set_valign(Gtk.Align.START)

	# contributors list
	lbl_contribs = Gtk.Label.new(_('| Contributors:'))
	lbl_contribs.set_valign(Gtk.Align.START)

	# box for credits
	box_credits = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
	box_credits.set_spacing(10)
	box_credits.set_valign(Gtk.Align.START)
	box_credits.append(lbl_patrons)
	box_credits.append(Gtk.Label.new(
		'yuanca\n'
		+ '\n'
		+ '\n'
	))
	box_credits.append(lbl_contribs)
	box_credits.append(Gtk.Label.new(
		'Åke Engelbrektson\n'
		+ 'Jürgen Benvenuti\n'
		+ 'manthanabc\n'
	))

	# box for about
	box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box.set_spacing(5)
	box.append(lbl_info)
	box.append(box_credits)

	# tab
	scr_about = Gtk.ScrolledWindow()
	scr_about.set_margin_start(10)
	scr_about.set_margin_end(10)
	scr_about.set_margin_top(10)
	scr_about.set_policy(Gtk.PolicyType.EXTERNAL, Gtk.PolicyType.EXTERNAL)
	scr_about.set_vexpand(True)
	scr_about.set_child(box)

	## tabbed notebook
	Interface.ntb_main = Gtk.Notebook()
	Interface.ntb_main.set_show_border(False)
	Interface.ntb_main.set_margin_bottom(5)
	Interface.ntb_main.append_page(Interface.box_playlists, Gtk.Label.new(_('Playlists')))
	Interface.ntb_main.append_page(box_search, Gtk.Label.new(_('Search')))
	Interface.ntb_main.append_page(scr_about, Gtk.Label.new(_('About')))
	Interface.ntb_main.connect('switch-page', _on_switch_page)
	for page in Interface.ntb_main.get_pages():
		page.set_property('tab-fill', True)
		page.set_property('tab-expand', True)

	# online mode toggle
	tog_online = Gtk.ToggleButton.new_with_label('')
	tog_online.set_halign(Gtk.Align.END)
	tog_online.set_tooltip_text(_('Toggle online mode'))
	tog_online.connect('toggled', _on_toggle_online)
	if Player.online:
		tog_online.set_active(True)
		tog_online.set_label(_('Online'))
	else:
		tog_online.set_active(False)
		tog_online.set_label(_('Offline'))

	# import button
	btn_import = Gtk.Button.new_with_label(_('Import'))
	btn_import.set_tooltip_text(_('Import from clipboard'))
	btn_import.connect('clicked', _on_import_playlist)

	# extra misc menu
	box_pop = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_pop.append(Gtk.LinkButton.new_with_label(
		'https://gitlab.com/zehkira/myuzi/-/issues',
		_('Report a bug')
	))
	box_pop.append(Gtk.LinkButton.new_with_label(
		'https://www.patreon.com/bePatron?u=65739770',
		_('Donate')
	))
	pop_more = Gtk.Popover.new()
	pop_more.set_child(box_pop)

	btn_more = Gtk.MenuButton()
	btn_more.set_icon_name('view-more')
	btn_more.set_popover(pop_more)

	## CSD - titlebar
	hbr_topbar = Gtk.HeaderBar()
	hbr_topbar.pack_start(btn_import)
	hbr_topbar.pack_end(btn_more)
	hbr_topbar.pack_end(tog_online)

	## toplevels
	box_main = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
	box_main.set_spacing(5)
	box_main.append(Interface.ntb_main)
	box_main.append(box_player)

	# app window
	Interface.window = Gtk.ApplicationWindow(application = app, title = 'Myuzi')
	Interface.window.set_default_size(600, 400)
	Interface.window.set_child(box_main)
	Interface.window.set_titlebar(hbr_topbar)

	build_playlists()
	Interface.window.show()

