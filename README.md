<img src='https://gitlab.com/zehkira/myuzi/-/raw/master/marketing/banner.png' alt='banner' width='200'><br>

Myuzi is a free and open source music streaming app for Linux.

Unlike Spotify, Myuzi:
- does not require an account,
- has no advertisements,
- is completely free,
- uses your system GTK theme.

All the music is streamed directly from YouTube.

Uses SponsorBlock data licensed used under CC BY-NC-SA 4.0 from https://sponsor.ajay.app/.

<br><a href='https://gitlab.com/zehkira/myuzi/-/blob/master/INSTALL.md#install-package'>
    <img src='https://gitlab.com/zehkira/myuzi/-/raw/master/marketing/download.png' alt='install from aur' width='220'>
</a>
&nbsp;
<a href='https://www.patreon.com/bePatron?u=65739770'>
    <img src='https://gitlab.com/zehkira/myuzi/-/raw/master/marketing/support.png' alt='patreon' width='220'>
</a>
<br><br>
<img src='https://gitlab.com/zehkira/myuzi/-/raw/master/marketing/screenshot.png' alt='screenshot' width='680'>
