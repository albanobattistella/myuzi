��    -      �  =   �      �     �  '   �          6     R     d     k     r     �     �     �     �     �  	   �     �     �  	             *     2     9     >     [     j     �  !   �     �     �     �  	                  6     K     \     i          �     �     �     �     �     �     �  <  �     4  8   P     �  !   �     �     �     �     �     	     	     1	  
   H	     S	  
   k	  
   v	     �	     �	  "   �	     �	     �	     �	  "   �	     �	     
     )
  (   I
  #   r
  #   �
  '   �
     �
     �
                >     Q     d     z          �     �     �     �     �     �                    +               *           $      !      &                            #      '            	       %   )       
      -      "   (                                                               ,    Add to playlist Autoplay similar songs after queue ends Can't search in offline mode Can't switch to online mode Connection failed Delete Donate Download error Downloading playlist... Downloading song... Export to clipboard Exported Import from clipboard Move down Move up New playlist... Next song No playlist data in clipboard Offline Online Play Play random songs from queue Playback error Playlist already exists Playlist creation failed Playlist data copied to clipboard Playlist download failed Playlist import failed Playlist rename failed Playlists Previous song Remove from local storage Remove from playlist Repeat this song Report a bug Save to local storage Search Searching for a song to play... Settings Song download failed Toggle online mode Toggle pause Unavailable Volume Project-Id-Version: Myuzi
PO-Revision-Date: 
Last-Translator: Åke Engelbrektson <eson@svenskasprakfiler.se>
Language-Team: Svenska Språkfiler
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1.1
 Lägg till i spelningslista Spela automatiskt upp liknande låtar när kön är slut Kan inte söka i offline-läge Kan inte växla till online-läge Anslutning misslyckades Ta bort Donera Nerladdningsfel Laddar ner spelningslista... Laddar ner låt... Exportera till urklipp Exporterad Importera från urklipp Flytta ner Flytta upp Ny spelningslista... Nästa låt Ingen spelningslistedata i urklipp Offline Online Spela Spela slumpmässig låt från kön Uppspelningsfel Spelningslistan finns redan Kunde inte skapa spelningslista Spelningslistedata kopierat till urklipp Kunde inte ladda ner spelningslista Kunde inte importera spelningslista Kunde inte byta namn på spelningslista Spelningslistor Föregående låt Ta bort från lokal lagring Ta bort från spelningslistan Upprepa denna låt Rapportera ett fel Spara i lokal lagring Sök Söker en låt att spela upp... Inställningar Kunde inte ladda ner låt Online-läge på/av Paus på/av Ej tillgänglig Volym 