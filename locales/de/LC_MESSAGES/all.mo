��    1      �  C   ,      8     9     ?  '   O     w     �     �     �     �     �     �     �               (     /  	   E     O     W  	   g     q     �     �     �     �     �     �     �     �  !   	     +     D     [  	   r     |     �     �     �     �     �     �     �          !     6     I     V     b     i  \  y     �     �  E   �  *   A	  3   l	     �	     �	     �	     �	  (   �	     
  !   -
  
   O
     Z
  "   f
     �
     �
     �
     �
  1   �
                     )  >   5     t  !   �  )   �  3   �  -     .   <  -   k     �     �  "   �  !   �                -     =  C   D     �  '   �     �     �     �     �     �        0          !                 $                                         '   ,                
          /           )       .      	   %       *   1            (                                       "   -   &             +   #        About Add to playlist Autoplay similar songs after queue ends Can't search in offline mode Can't switch to online mode Connection failed Delete Donate Download error Downloading playlist... Downloading song... Export to clipboard Exported Import Import from clipboard Move down Move up New playlist... Next song No playlist data in clipboard Offline Online Patrons: Play Play random songs from queue Playback error Playlist already exists Playlist creation failed Playlist data copied to clipboard Playlist download failed Playlist import failed Playlist rename failed Playlists Previous song Remove from local storage Remove from playlist Repeat this song Report a bug Save to local storage Search Searching for a song to play... Searching... Song download failed Toggle online mode Toggle pause Unavailable Volume | Contributors: Project-Id-Version: myuzi
PO-Revision-Date: 
Last-Translator: Jürgen Benvenuti <gastornis@posteo.org>
Language-Team: German <gnome-de@gnome.org>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.1.1
X-Poedit-SourceCharset: UTF-8
 Info Zur Wiedergabeliste hinzufügen Automatisch ähnliche Titel wiedergeben, wenn die Warteschlange endet Im Offline-Modus kann nicht gesucht werden Es kann nicht in den Online-Modus gewechselt werden Verbindung fehlgeschlagen Löschen Spenden Fehler beim Herunterladen Wiedergabeliste wird heruntergeladen … Titel wird heruntergeladen … In die Zwischenablage exportieren Exportiert Importieren Aus der Zwischenablage importieren Nach unten verschieben Nach oben verschieben Neue Wiedergabeliste … Nächster Titel Keine Wiedergabeliste-Daten in der Zwischenablage Offline Online Patrons: Wiedergeben Zufällig ausgewählte Titel aus der Warteschlange wiedergeben Fehler bei der Wiedergabe Wiedergabeliste existiert bereits Fehler beim Erstellen der Wiedergabeliste Wiedergabeliste-Daten in die Zwischenablage kopiert Fehler beim Herunterladen der Wiedergabeliste Importieren der Wiedergabeliste fehlgeschlagen Umbenennen der Wiedergabeliste fehlgeschlagen Wiedergabelisten Vorheriger Titel Aus dem lokalen Speicher entfernen Aus der Wiedergabeliste entfernen Diesen Titel wiederholen Einen Fehler melden Lokal speichern Suchen Es wird nach einem Titel gesucht, der wiedergegeben werden kann … Suche läuft … Herunterladen des Titels fehlgeschlagen Online/Offline Wiedergabe/Pause Nicht verfügbar Lautstärke | Beitragende: 